                                          2020.2.0b14 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                          \       ŕyŻ     `                                                                                                                                                ŕyŻ                                                                                    SingleSourceDistances     using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Msagl.Core;
using Microsoft.Msagl.Core.GraphAlgorithms;
using Microsoft.Msagl.Core.Layout;

namespace Microsoft.Msagl.Layout.MDS {
    /// <summary>
    /// Provides functionality for computing distances in a graph.
    /// </summary>
    public class SingleSourceDistances : AlgorithmBase {
        private GeometryGraph graph;

        private Node source;

        private bool directed;

        /// <summary>
        /// Dijkstra algorithm. Computes graph-theoretic distances from a node to
        /// all other nodes in a graph with nonnegative edge lengths.
        /// The distance between a node and itself is 0; the distance between a pair of
        /// nodes for which no connecting path exists is Double.PositiveInfinity.
        /// </summary>
        /// <param name="graph">A graph.</param>
        /// <param name="source">The source node.</param>
        /// <param name="directed">Whether the graph is directed.</param>
        public SingleSourceDistances(GeometryGraph graph, Node source, bool directed)
        {
            this.graph = graph;
            this.source = source;
            this.directed = directed;
        }

        /// <summary>
        /// An array of distances from the source node to all nodes.
        /// Nodes are indexed in their natural order when iterating over them.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Justification = "This is performance critical.  Copying the array would be slow.")]
        public double[] Result { get; private set; }

        /// <summary>
        /// Executes the algorithm.
        /// </summary>
        protected override void RunInternal() {
            this.StartListenToLocalProgress(graph.Nodes.Count);

            Result = new double[graph.Nodes.Count];

            var q = new Microsoft.Msagl.Core.DataStructures.GenericBinaryHeapPriorityQueue<Node>();
            Dictionary<Node, double> d = new Dictionary<Node, double>();
            foreach (Node node in graph.Nodes) {
                q.Enqueue(node, Double.PositiveInfinity);
                d[node] = Double.PositiveInfinity;
            }
            q.DecreasePriority(source, 0);

            while (q.Count>0) {
                
                ProgressStep();

                double prio;
                Node u = q.Dequeue(out prio);
                d[u] = prio;
                IEnumerator<Edge> enumerator;
                if (directed)
                    enumerator = u.OutEdges.GetEnumerator();
                else
                    enumerator = u.Edges.GetEnumerator();
                while (enumerator.MoveNext()) {
                    Edge uv = enumerator.Current;
                    Node v = uv.Target;
                    if (u == v)
                        v = uv.Source;
                    // relaxation step
                    if (d[v] > d[u] + uv.Length) {
                        d[v] = d[u] + uv.Length;
                        q.DecreasePriority(v, d[v]);
                    }
                }
            }
            int i = 0;
            foreach (Node v in graph.Nodes) {
#if SHARPKIT //https://github.com/SharpKit/SharpKit/issues/7 out keyword not working with arrays
                double dummy;
                if (!d.TryGetValue(v, out dummy))
                    dummy = Double.PositiveInfinity;
                Result[i] = dummy;
#else
                if (!d.TryGetValue(v, out Result[i]))
                    Result[i] = Double.PositiveInfinity;
#endif
                i++;
            }
        }
    }
}
                        SingleSourceDistances      Microsoft.Msagl.Layout.MDS  