                           "<               2020.2.0b14 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                          \       ŕyŻ     `       Ü                                                                                                                                        ŕyŻ                                                                                    ScanDirection   K  //
// ScanDirection.cs
// MSAGL ScanDirection class for Rectilinear Edge Routing line generation.
//
// Copyright Microsoft Corporation.
using Microsoft.Msagl.Core.Geometry;
using Microsoft.Msagl.Routing.Visibility;

namespace Microsoft.Msagl.Routing.Rectilinear {
    // Encapsulate a direction and some useful values derived therefrom.
    internal class ScanDirection {
        // The direction of primary interest, either the direction of the sweep (the
        // coordinate the scanline sweeps "up" in) or along the scan line ("sideways"
        // to the sweep direction, scanning for obstacles).
        internal Directions Direction { get; private set; }
        internal Point DirectionAsPoint { get; private set; }

        // The perpendicular direction - opposite of comments for Direction.
        internal Directions PerpDirection { get; private set; }
        internal Point PerpDirectionAsPoint { get; private set; }

        // The oppposite direction of the primary direction.
        internal Directions OppositeDirection { get; private set; }

        // Use the internal static xxxInstance properties to get an instance.
        private ScanDirection(Directions directionAlongScanLine) {
            System.Diagnostics.Debug.Assert(StaticGraphUtility.IsAscending(directionAlongScanLine),
                "directionAlongScanLine must be ascending");
            Direction = directionAlongScanLine;
            DirectionAsPoint = CompassVector.ToPoint(Direction);
            PerpDirection = (Directions.North == directionAlongScanLine) ? Directions.East : Directions.North;
            PerpDirectionAsPoint = CompassVector.ToPoint(PerpDirection);
            OppositeDirection = CompassVector.OppositeDir(directionAlongScanLine);
        }

        internal bool IsHorizontal { get { return Directions.East == Direction; } }
        internal bool IsVertical { get { return Directions.North == Direction; } }

        // Compare in perpendicular direction first, then parallel direction.
        internal int Compare(Point lhs, Point rhs) {
            int cmp = ComparePerpCoord(lhs, rhs);
            return (0 != cmp) ? cmp : CompareScanCoord(lhs, rhs);
        }

        internal int CompareScanCoord(Point lhs, Point rhs) {
            return PointComparer.Compare((lhs - rhs) * DirectionAsPoint, 0.0);
        }
        internal int ComparePerpCoord(Point lhs, Point rhs) {
            return PointComparer.Compare((lhs - rhs) * PerpDirectionAsPoint, 0.0);
        }

        internal bool IsFlat(SegmentBase seg) {
            return IsFlat(seg.Start, seg.End);
        }
        internal bool IsFlat(Point start, Point end) {
            // Return true if there is no change in the perpendicular direction.
            return PointComparer.Equal((end - start) * PerpDirectionAsPoint, 0.0);
        }
        internal bool IsPerpendicular(SegmentBase seg) {
            return IsPerpendicular(seg.Start, seg.End);
        }
        internal bool IsPerpendicular(Point start, Point end) {
            // Return true if there is no change in the primary direction.
            return PointComparer.Equal((end - start) * DirectionAsPoint, 0.0);
        }

        internal double Coord(Point point) {
            return point * DirectionAsPoint;
        }

        internal Point Min(Point first, Point second) {
            return (Compare(first, second) <= 0) ? first : second;
        }
        internal Point Max(Point first, Point second) {
            return (Compare(first, second) >= 0) ? first : second;
        }

// ReSharper disable InconsistentNaming
        private static readonly ScanDirection horizontalInstance = new ScanDirection(Directions.East);
        private static readonly ScanDirection verticalInstance = new ScanDirection(Directions.North);
// ReSharper restore InconsistentNaming
        internal static ScanDirection HorizontalInstance { get { return horizontalInstance; } }
        internal static ScanDirection VerticalInstance { get { return verticalInstance; } }
        internal ScanDirection PerpendicularInstance { get { return IsHorizontal ? VerticalInstance : HorizontalInstance; } }
        static internal ScanDirection GetInstance(Directions dir) { return StaticGraphUtility.IsVertical(dir) ? VerticalInstance : HorizontalInstance; }

        /// <summary/>
        public override string ToString() {
            return Direction.ToString();
        }
    }
}                        ScanDirection   #   Microsoft.Msagl.Routing.Rectilinear 