                           %               2020.2.0b14 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                          \       ŕyŻ     `       ´                                                                                                                                        ŕyŻ                                                                                    TopologicalSort "  using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Msagl.Core.GraphAlgorithms{
    /// <summary>
    /// Implements the topological sort
    /// </summary>
    public static class TopologicalSort{
        /// <summary>
        /// Do a topological sort of a list of int edge tuples
        /// </summary>
        /// <param name="numberOfVertices">number of vertices</param>
        /// <param name="edges">edge pairs</param>
        /// <returns>ordered node indexes</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static int[] GetOrder(int numberOfVertices, IEnumerable<System.Tuple<int,int>> edges)
        {
            var dag = new BasicGraphOnEdges<IntPair>(from e in edges select new IntPair(e.Item1, e.Item2), numberOfVertices);
            return GetOrder(dag);
        }

        /// <summary>
        /// The function returns an array arr such that
        /// arr is a permutation of the graph vertices,
        /// and for any edge e in graph if e.Source=arr[i]
        /// e.Target=arr[j], then i is less than j
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        internal static int[] GetOrder<TEdge>(BasicGraphOnEdges<TEdge> graph) where TEdge : IEdge{
            var visited = new bool[graph.NodeCount];

            //no recursion! So we have to organize a stack
            var sv = new Stack<int>();
            var se = new Stack<IEnumerator<int>>();

            var order = new List<int>();

            IEnumerator<int> en;
            for (int u = 0; u < graph.NodeCount; u++){
                if (visited[u])
                    continue;

                int cu = u;
                visited[cu] = true;
                en = graph.OutEdges(u).Select(e => e.Target).GetEnumerator();

                do{
                    while (en.MoveNext()){
                        int v = en.Current;
                        if (!visited[v]){
                            visited[v] = true;
                            sv.Push(cu);
                            se.Push(en);
                            cu = v;
                            en = graph.OutEdges(cu).Select(e => e.Target).GetEnumerator();
                        }
                    }
                    order.Add(cu);


                    if (sv.Count > 0){
                        en = se.Pop();
                        cu = sv.Pop();
                    }
                    else
                        break;
                } while (true);
            }
            order.Reverse();
            return order.ToArray();
        }

        /// <summary>
        /// The function returns an array arr such that
        /// arr is a permutation of edge  vertices,
        /// and for any edge e from the list, if e.Source=arr[i]
        /// e.Target=arr[j], then i is less than j
        /// </summary>
        /// <returns></returns>
        internal static int[] GetOrderOnEdges<TEdge>(IEnumerable<TEdge> edges) where TEdge : IEdge {
            var visited = new Dictionary<int,bool>();
            var graph = new Dictionary<int, List<int>>();
            foreach (var e in edges) {
                visited[e.Source]=visited[e.Target]=false;
                var x=e.Source;
                List<int> list;
                if(! graph.TryGetValue(x, out list)) {
                   graph[x]=list=new List<int>();
                }
                list.Add(e.Target);
            }
            //organize a couple of stacks to avoid the recursion
            var sv = new Stack<int>();
            var se = new Stack<IEnumerator<int>>();

            var order = new List<int>();

            IEnumerator<int> en;
            foreach (var  u in visited.Keys.ToArray()) {
                if (visited[u])
                    continue;

                int cu = u;

                visited[cu] = true;
                List<int> glist;
                if (!graph.TryGetValue(u, out glist)) continue;
                if (glist == null) continue;
                en = glist.GetEnumerator();
                do {
                    while (en.MoveNext()) {
                        int v = en.Current;
                        if (!visited[v]) {
                            visited[v] = true;
                            List<int> list;
                            if (!graph.TryGetValue(v, out list)) {
                                order.Add(v);//it is a leaf
                                continue; 
                            }
                            sv.Push(cu);
                            se.Push(en);
                            cu = v;
                            en = list.GetEnumerator();
                        }
                    }
                    order.Add(cu);

                    if (sv.Count > 0) {
                        en = se.Pop();
                        cu = sv.Pop();
                    } else
                        break;
                } while (true);
            }
            order.Reverse();
            return order.ToArray();
        }
    }
}                         TopologicalSort $   Microsoft.Msagl.Core.GraphAlgorithms