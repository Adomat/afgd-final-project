                           T               2020.2.0b14 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                          \       ŕyŻ     `       ô                                                                                                                                        ŕyŻ                                                                                    ObstaclePortj  //
// ObstaclePort.cs
// MSAGL class for ObstaclePorts for Rectilinear Edge Routing.
//
// Copyright Microsoft Corporation.

using System.Collections.Generic;
using Microsoft.Msagl.Core.Geometry;
using Microsoft.Msagl.Core.Geometry.Curves;
using Microsoft.Msagl.Core.Layout;
using Microsoft.Msagl.Routing.Visibility;

namespace Microsoft.Msagl.Routing.Rectilinear {
    internal class ObstaclePort {
        internal Port Port { get; private set; }
        internal Obstacle Obstacle { get; private set; }

        internal VisibilityVertex CenterVertex;

        // These are derived from PortEntry spans if present, else from Port.Location.
        internal List<ObstaclePortEntrance> PortEntrances { get; private set; }

        internal bool HasCollinearEntrances { get; private set; }

        // Hang onto this separately to detect port movement.
        internal Point Location { get; private set; }

        internal Rectangle VisibilityRectangle = Rectangle.CreateAnEmptyBox();

        internal ObstaclePort(Port port, Obstacle obstacle) {
            this.Port = port;
            this.Obstacle = obstacle;
            this.PortEntrances = new List<ObstaclePortEntrance>();
            this.Location = ApproximateComparer.Round(this.Port.Location);
        }

        internal void CreatePortEntrance(Point unpaddedBorderIntersect, Directions outDir, ObstacleTree obstacleTree) {
            var entrance = new ObstaclePortEntrance(this, unpaddedBorderIntersect, outDir, obstacleTree);
            PortEntrances.Add(entrance);
            this.VisibilityRectangle.Add(entrance.MaxVisibilitySegment.End);
#if SHARPKIT //https://code.google.com/p/sharpkit/issues/detail?id=370
            this.HasCollinearEntrances = this.HasCollinearEntrances | entrance.IsCollinearWithPort;
#else
            this.HasCollinearEntrances |= entrance.IsCollinearWithPort;
#endif
        }

        internal void ClearVisibility() {
            // Most of the retained PortEntrance stuff is about precalculated visibility.
            this.PortEntrances.Clear();
        }

        internal void AddToGraph(TransientGraphUtility transUtil, bool routeToCenter) {
            // We use only border vertices if !routeToCenter.
            if (routeToCenter) {
                CenterVertex = transUtil.FindOrAddVertex(this.Location);
            }
        }

        internal void RemoveFromGraph() {
            // Currently all transient removals and edge restorations are done by TransientGraphUtility itself.
            foreach (var entrance in PortEntrances) {
                entrance.RemoveFromGraph();
            }
            CenterVertex = null;
        }

        // PortManager will recreate the Port if it detects this (this.Location has already been rounded).
        internal bool LocationHasChanged { get { return !PointComparer.Equal(this.Location, ApproximateComparer.Round(this.Port.Location)); } }

        /// <summary>
        /// The curve associated with the port.
        /// </summary>
        public ICurve PortCurve { get { return this.Port.Curve; } }

        /// <summary>
        /// The (unrounded) location of the port.
        /// </summary>
        public Point PortLocation { get { return this.Port.Location; } }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return this.Port + this.Obstacle.ToString();
        }
    }
}                         ObstaclePort#   Microsoft.Msagl.Routing.Rectilinear 