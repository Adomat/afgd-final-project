﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.Visualization
{
    public class SimpleCameraRotationScript : MonoBehaviour
    {
        public float RotationSpeed = 10.0f;

        private Vector3 rotationcenter;

        // Start is called before the first frame update
        void Start()
        {
            GameObject spawn = GameObject.Find("VisualizationModelParent");
            if (spawn != null)
                rotationcenter = spawn.transform.position;
            else
                rotationcenter = new Vector3(5, 1, 5);
        }

        // Update is called once per frame
        void Update()
        {
            transform.RotateAround(rotationcenter, new Vector3(0.0f, 1.0f, 0.0f), RotationSpeed * Time.deltaTime);
        }
    }
}