using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;

using Microsoft.Msagl.Core;
using Microsoft.Msagl.Core.Geometry;
using Microsoft.Msagl.Core.GraphAlgorithms;
using Microsoft.Msagl.Layout.Incremental;
using Microsoft.Msagl.Core.Layout;
using Microsoft.Msagl.Core.Geometry.Curves;

namespace ISA.CityGeneration
{
    public class City
    {
        public List<Building> buildings { private set; get; }
        public List<Street> streets { private set; get; }

        public float NarrowestStreetWidth { get { return streets[streets.Count - 1].width; } }
        public float WidestStreetWidth { get { return streets[0].width; } }



        public City(int numberOfBuildings, bool visualDebug)
        {
            buildings = new List<Building>();
            for (int i = 0; i < numberOfBuildings; i++)
            {
                Building newBuilding = new Building(this);
                buildings.Add(newBuilding);
            }

            streets = new List<Street>();
            for (int i = 0; i < buildings.Count; i++)
            {
                Building building = buildings[i];
                for (int j = i + 1; j < buildings.Count; j++)
                {
                    Building otherBuilding = buildings[j];

                    if(RandomFactory.INSTANCE.getRandomBool(0.3f))
                    {
                        Street street = new Street(this, building, otherBuilding);
                        street.width = RandomFactory.INSTANCE.getRandomFloat(1f, 10f);
                        streets.Add(street);
                    }
                }
            }
            streets.Sort();
            streets.Reverse();


            //placeBuildingsRandomly();
            placeBuildingsInCircle(1f);
            placeBuildingsBasedOnGraphLayout();

            if (visualDebug)
            {
                foreach (Street street in streets)
                {
                    Vector3 pos1 = new Vector3(street.building1.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, street.building1.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Vector3 pos2 = new Vector3(street.building2.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, street.building2.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Debug.DrawLine(pos1, pos2, Color.HSVToRGB(0, 0, 0.25f), float.MaxValue);
                }
                foreach (Building building in buildings)
                {
                    Vector3 pos = new Vector3(building.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, building.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Debug.DrawLine(pos, pos + Vector3.up * 0.5f, building.Color, float.MaxValue);
                }
            }

            foreach (Building building in buildings)
            {
                building.GenerateGarden();
            }
        }

        private void placeBuildingsInCircle(float range)
        {
            for(int i=0; i<buildings.Count; i++)
            {
                Vector2 relativePoint = CityGenerator.CalculatePositionInCircle01(i, buildings.Count);
                Vector2 absolutePoint = (0.5f * Vector2.one + 0.4f * relativePoint) * range;
                buildings[i].InitialVoronoiCenterPoint = absolutePoint;
            }
        }

        private void placeBuildingsBasedOnGraphLayout()
        {
            Dictionary<Building, Node> nodeMapping = new Dictionary<Building, Node>();

            GeometryGraph graph = new GeometryGraph();
            foreach(Building building in buildings)
            {
                Node n = new Node(CurveFactory.CreateRectangle(3, 3, new Point()));
                n.Center = new Point(building.InitialVoronoiCenterPoint.x, building.InitialVoronoiCenterPoint.y);
                //n.BoundingBox = new Rectangle(new Point(-1, -1), new Point(1, 1));
                n.UserData = building;

                graph.Nodes.Add(n);
                nodeMapping.Add(building, n);
            }

            foreach(Street street in streets)
            {
                Edge e = new Edge(nodeMapping[street.building1], nodeMapping[street.building2]);
                e.Weight = (int) Mathf.Round(street.width);
                graph.Edges.Add(e);
            }
            graph.UpdateBoundingBox();
            //graph.RootCluster = new Cluster(new Point(CityGenerator.VORONOI_DISTANCE_01 / 2, CityGenerator.VORONOI_DISTANCE_01 / 2));

            FastIncrementalLayoutSettings settings;
            //settings = FastIncrementalLayoutSettings.CreateFastIncrementalLayoutSettings();
            settings = new FastIncrementalLayoutSettings
            {
                ApplyForces = true, // false
                ApproximateRepulsion = true,
                ApproximateRouting = true,
                AttractiveForceConstant = 1.0,
                AttractiveInterClusterForceConstant = 1.0,
                AvoidOverlaps = true,
                ClusterGravity = 1.0,
                Decay = 0.9,
                DisplacementThreshold = 0.00000005,
                Friction = 0.8,
                GravityConstant = 1.0,
                InitialStepSize = 2.0,
                InterComponentForces = false,
                Iterations = 0,
                LogScaleEdgeForces = false,
                MaxConstraintLevel = 2,
                MaxIterations = 20,
                MinConstraintLevel = 0,
                MinorIterations = 1,
                ProjectionIterations = 5,
                RepulsiveForceConstant = 2.0,
                RespectEdgePorts = false,
                RouteEdges = false,
                RungeKuttaIntegration = true,
                UpdateClusterBoundariesFromChildren = true,
                NodeSeparation = 20
            };

            Microsoft.Msagl.Miscellaneous.LayoutHelpers.CalculateLayout(graph, settings, null);

            //settings.structuralConstraints.Add(new TestConstraint(graph.Nodes));
            //settings.IncrementalRun(graph);
            //FastIncrementalLayout layout = new FastIncrementalLayout(graph, settings, settings.MinConstraintLevel, anyCluster => settings);
            //layout.Run();

            float smallestPosition = float.MaxValue;
            float largestPosition = float.MinValue;
            foreach (Node node in graph.Nodes)
            {
                if ((float)node.Center.X < smallestPosition)
                    smallestPosition = (float)node.Center.X;
                if ((float)node.Center.Y < smallestPosition)
                    smallestPosition = (float)node.Center.Y;

                if ((float)node.Center.X > largestPosition)
                    largestPosition = (float)node.Center.X;
                if ((float)node.Center.Y > largestPosition)
                    largestPosition = (float)node.Center.Y;
            }

            // normalize to 0-1 range
            foreach (Node node in graph.Nodes)
            {
                float newX = ((float)node.Center.X - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                float newY = ((float)node.Center.Y - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                Vector2 position01 = new Vector2(newX, newY) * 0.8f + 0.1f * Vector2.one;
                ((Building)node.UserData).InitialVoronoiCenterPoint = position01;
            }
        }

        /*private class TestConstraint : IConstraint
        {
            private IList<Node> nodes;

            public Test(IList<Node> nodes)
            {
                this.nodes = nodes;
            }

            public IEnumerable<Node> Nodes => nodes;

            public int Level => 2;

            public double Project()
            {
                return 0;
            }
        }*/

        private void placeBuildingsRandomly()
        {
            for (int i = 0; i < buildings.Count; i++)
            {
                Building building = buildings[i];

                Vector2 position = Vector2.zero;
                int positioningAttempt = 0;
            TryNewPosition:
                position = new Vector2(RandomFactory.INSTANCE.getRandomFloat(0.1f, 0.9f), RandomFactory.INSTANCE.getRandomFloat(0.1f, 0.9f));
                foreach (Building otherBuilding in buildings)
                {
                    if (Vector2.Distance(position, otherBuilding.InitialVoronoiCenterPoint) <= 3f * CityGenerator.VORONOI_DISTANCE_01)
                    {
                        if (positioningAttempt == 100)
                        {
                            Debug.LogError("Could not position a building without colliding with another after several failed attempts!");
                            break;
                        }
                        // too little space, try again
                        positioningAttempt++;
                        goto TryNewPosition;
                    }
                }

                building.InitialVoronoiCenterPoint = position;
            }
        }

        /*private Building[] getClosestBuildings(Building building, int searchedAmount)
        {
            if (buildings.Count - 1 <= searchedAmount)
                throw new System.Exception("Searching for more buildings than possible!");

            Building[] result = new Building[searchedAmount];
            float[] bestDistances = new float[searchedAmount];

            Vector2 center = building.CenterPoint;
            foreach (Building otherBuilding in buildings)
            {
                if (otherBuilding == building)
                    continue;
                Vector2 otherCenter = otherBuilding.CenterPoint;

            }

            return result;
        }*/



        public List<VoronoiDiagramSite<VoronoiSiteData>> CalculateVoronoiPoints(int voronoiDimensions)
        {
            List<VoronoiDiagramSite<VoronoiSiteData>> voronoiPoints = new List<VoronoiDiagramSite<VoronoiSiteData>>();

            // First, get the points for each building!
            foreach(Building building in buildings)
            {
                Vector2 pos = building.InitialVoronoiCenterPoint * (voronoiDimensions-1);
                voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(building, VoronoiSiteData.Type.Building)));

                foreach (Vector2 garden in building.GardenPoints)
                {
                    pos = garden * (voronoiDimensions - 1);
                    voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(building, VoronoiSiteData.Type.Garden)));
                }
            }

            int sizeBeforeFilling = voronoiPoints.Count;
            int pointsToCreateMax = 10000;

            // Then, fill out additional points to make the city more interesting :)
            while(true)
            {
                int attempts = 0;
            MakeNewPoint:
                Vector2 pos = new Vector2(RandomFactory.INSTANCE.getRandomFloat(), RandomFactory.INSTANCE.getRandomFloat()) * (voronoiDimensions - 1);
                foreach (VoronoiDiagramSite<VoronoiSiteData> voronoiPoint in voronoiPoints)
                {
                    if (Vector2.Distance(pos, voronoiPoint.Coordinate) <= 1f * CityGenerator.VORONOI_DISTANCE_01 * (voronoiDimensions - 1))
                    {
                        if (attempts == 100)
                        {
                            // tried 100 times without success, that's good enough :)
                            //Debug.Log("Can't find any more available space to fill in points in the voronoi! Filled in " + (voronoiPoints.Count - sizeBeforeFilling) + " points!");
                            return voronoiPoints;
                        }
                        // too little space, try again
                        attempts++;
                        goto MakeNewPoint;
                    }
                }

                voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(null, VoronoiSiteData.Type.Free)));
                if(--pointsToCreateMax == 0)
                {
                    Debug.Log("Done! Filled in " + (voronoiPoints.Count - sizeBeforeFilling) + " points in the voronoi!");
                    return voronoiPoints;
                }
            }
        }


        internal void GenerateBuildingMeshes(GameObject rootGameObject)
        {
            GameObject buildingsObject = new GameObject("Buildings");
            buildingsObject.transform.parent = rootGameObject.transform;
            foreach (Building building in buildings)
            {
                building.GenerateMesh(buildingsObject);
            }
        }
    }
}