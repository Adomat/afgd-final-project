﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using System;

namespace ISA.CityGeneration
{
    public class CityGenerator : MonoBehaviour
    {
        internal static readonly float VORONOI_DISTANCE_01 = 0.02f;

        internal static readonly int CITY_DIMENSIONS = 10;
        internal static readonly int VORONOI_DIMENSIONS = 100000;

        [SerializeField]
        private int numberOfBuildings = 10;
        private City city;



        internal bool IsBuildingAt(Vector3 position, float maxDistance)
        {
            foreach(Building building in city.buildings)
                if (Vector3.Distance(ConvertVoronoiSpaceToWorldSpace3D(building.VoronoiSite.Centroid), position) < maxDistance)
                    return true;
            return false;
        }



        private void Start()
        {
            city = new City(numberOfBuildings, false);

            int width = VORONOI_DIMENSIONS;
            int height = VORONOI_DIMENSIONS;

            VoronoiDiagram<VoronoiSiteData> voronoiDiagram = new VoronoiDiagram<VoronoiSiteData>(new Rect(0f, 0f, width, height));
            voronoiDiagram.AddSites(city.CalculateVoronoiPoints(VORONOI_DIMENSIONS));
            voronoiDiagram.GenerateSites(2);

            /*
            // Saving image to file
            // DON'T USE! Image will be too large to save!!!!!!!!!!!

            var outImg = new Texture2D(width, height);
            outImg.SetPixels(voronoiDiagram.Get1DSampleArray());
            outImg.Apply();

            System.IO.File.WriteAllBytes("diagram.png", outImg.EncodeToPNG());
            */

            assignVoronoiSitesToBuildings(voronoiDiagram, false);

            foreach (Street street in city.streets)
                street.GenerateAndUpdateSegments(voronoiDiagram);

            GameObject streetParentObject = new GameObject("Streets");
            streetParentObject.transform.parent = transform;
            for (int i = 0; i < city.streets.Count; i++)
            {
                GameObject streetObject = new GameObject("Streets #"+(i+1));
                streetObject.transform.parent = streetParentObject.transform;
                if(!city.streets[i].GenerateMesh(streetObject, (1f - (float)i / city.streets.Count) / 1000f))
                {
                    // The generation did not result in any new street structures.
                    // Remove this container object to not fill the object hierarchy with useless empty objects.
                    Destroy(streetObject);
                }
            }

            city.GenerateBuildingMeshes(gameObject);

            (new ReflectionProbeHandler()).CreateReflectionProbes(this, new Vector2(CITY_DIMENSIONS, CITY_DIMENSIONS), 5);
        }



        private void assignVoronoiSitesToBuildings(VoronoiDiagram<VoronoiSiteData> voronoiDiagram, bool visualDebug)
        {
            // Assign each voronoi site (the tiles) to a building and also keep track of
            // to what part the site belongs (i.e., center, building complex, or garden).
            foreach (KeyValuePair<int, VoronoiDiagramGeneratedSite<VoronoiSiteData>> site in voronoiDiagram.GeneratedSites)
            {
                if(visualDebug)
                    visuallyDebugSite(voronoiDiagram, site.Value);

                Building building = site.Value.SiteData.building;
                switch (site.Value.SiteData.type)
                {
                    case VoronoiSiteData.Type.Building:
                        building.VoronoiSite = site.Value;
                        if (visualDebug)
                        {
                            Vector3 pos = new Vector3(building.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, building.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                            Debug.DrawLine(pos, pos + Vector3.up * 0.5f, building.Color, float.MaxValue);
                        }
                        break;
                    case VoronoiSiteData.Type.Garden:
                        building.GardenVoronoiSites.Add(site.Value);
                        break;
                }
            }
        }



        private void visuallyDebugSite(VoronoiDiagram<VoronoiSiteData> voronoiDiagram, VoronoiDiagramGeneratedSite<VoronoiSiteData> site)
        {
            Vector3 startPoint = ConvertVoronoiSpaceToWorldSpace3D(site.Centroid);
            Vector3 endPoint = startPoint + Vector3.up * CITY_DIMENSIONS / 100;
            //Debug.DrawLine(startPoint, endPoint, Color.white, float.MaxValue);

            foreach (VoronoiDiagramGeneratedEdge edge in site.Edges)
            {
                startPoint = ConvertVoronoiSpaceToWorldSpace3D(edge.LeftEndPoint);
                endPoint = ConvertVoronoiSpaceToWorldSpace3D(edge.RightEndPoint);

                Color color = Color.HSVToRGB(0, 0, 0.25f);
                /*if (site.SiteData.type == VoronoiSiteData.Type.Garden)
                    color = Color.green;*/
                Debug.DrawLine(startPoint, endPoint, color, float.MaxValue);
            }

            foreach (int neighborIndex in site.NeighborSites)
            {
                VoronoiDiagramGeneratedSite<VoronoiSiteData> neighbor = voronoiDiagram.GeneratedSites[neighborIndex];
                if (neighbor.IsEdge)
                    continue;

                startPoint = ConvertVoronoiSpaceToWorldSpace3D(site.Centroid);
                endPoint = ConvertVoronoiSpaceToWorldSpace3D(neighbor.Centroid);
                Debug.DrawLine(startPoint, endPoint, Color.HSVToRGB(0, 0, 0.1f), float.MaxValue);
            }
        }

        public static float Convert01LengthToWorldSpace(float length01)
        {
            return length01 * (float)CITY_DIMENSIONS;
        }

        public static float ConvertVoronoiLengthToWorldSpace(float voronoiLength)
        {
            return voronoiLength * ((float)CITY_DIMENSIONS / (float)VORONOI_DIMENSIONS);
        }

        public static Vector2 ConvertVoronoiSpaceToWorldSpace2D(Vector2 voronoiPos)
        {
            return new Vector2(voronoiPos.x, voronoiPos.y) * ((float)CITY_DIMENSIONS / (float)VORONOI_DIMENSIONS); // - new Vector3((float)cityDimensions / 2, 0, (float)cityDimensions / 2);
        }

        public static Vector3 ConvertVoronoiSpaceToWorldSpace3D(Vector2 voronoiPos)
        {
            Vector2 result = ConvertVoronoiSpaceToWorldSpace2D(voronoiPos);
            return new Vector3(result.x, 0, result.y);
        }

        internal static Vector2 CalculatePositionInCircle01(int index, int max)
        {
            float angle = (float)index * 360.0f / (float)max;
            return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        }



        internal static GameObject InstantiateGameObject(string pathToResource)
        {
            return Instantiate(Resources.Load(pathToResource) as GameObject);
        }

        internal static Material InstantiateMaterial(string pathToResource)
        {
            return Instantiate(Resources.Load(pathToResource) as Material);
        }
    }
}