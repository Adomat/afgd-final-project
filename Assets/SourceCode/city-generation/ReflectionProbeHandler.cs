using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace ISA.CityGeneration
{
    public class ReflectionProbeHandler
    {
        public void CreateReflectionProbes(CityGenerator callingGenerator, Vector2 space, int resolution)
        {
            GameObject probesParentGameObject = new GameObject("Reflection Probes");
            probesParentGameObject.transform.parent = callingGenerator.gameObject.transform;

            for (int x = 0; x < resolution; x++)
            {
                for (int z = 0; z < resolution; z++)
                {
                    Vector3 position = new Vector3(((float)x / resolution) * space.x, 0.5f, ((float)z / resolution) * space.y);
                    if (!callingGenerator.IsBuildingAt(position, 2f))
                        continue;

                    GameObject probeGameObject = new GameObject("Reflection Probe ["+ ((float)x / resolution) * space.x + ","+ ((float)z / resolution) * space.y + "]");
                    probeGameObject.transform.parent = probesParentGameObject.gameObject.transform;
                    probeGameObject.transform.position = position;

                    ReflectionProbe probeComponent = probeGameObject.AddComponent<ReflectionProbe>() as ReflectionProbe;
                    probeComponent.resolution = 256;
                    probeComponent.size = new Vector3((1f / resolution) * space.x, 10, (1f / resolution) * space.y);
                    probeComponent.hdr = true;
                    probeComponent.mode = ReflectionProbeMode.Realtime;
                    probeComponent.refreshMode = ReflectionProbeRefreshMode.ViaScripting;
                    probeComponent.timeSlicingMode = ReflectionProbeTimeSlicingMode.AllFacesAtOnce;

                    probeComponent.RenderProbe();
                }
            }
        }
    }
}
