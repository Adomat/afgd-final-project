using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.CityGeneration
{
    public class BuildingGarden : AbstractBuildingComposite
    {
        public Building ParentBuilding { get; private set; }




        public BuildingGarden(Building Parent)
        {
            this.ParentBuilding = Parent;
        }
    }
}