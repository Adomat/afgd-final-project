﻿using System.Collections;
using UnityEngine;

namespace ISA.CityGeneration
{
    public class RandomFactory
    {

        public static RandomFactory INSTANCE = new RandomFactory(24061337);



        private System.Random rand;

        public RandomFactory(int seed)
        {
            rand = new System.Random(seed);
        }



        public float getRandomFloat()
        {
            return (float)rand.NextDouble();
        }

        public float getRandomFloat(float max)
        {
            return getRandomFloat() * max;
        }

        public float getRandomFloat(float min, float max)
        {
            if (max <= min)
                throw new System.Exception("max value must be larger than min value!");
            return min + getRandomFloat(max-min);
        }



        public int getRandomInt()
        {
            return rand.Next();
        }
        public int getRandomInt(int max)
        {
            return rand.Next(max);
        }
        public int getRandomInt(int min, int max)
        {
            return rand.Next(min, max);
        }



        public bool getRandomBool(float probability)
        {
            return rand.NextDouble() <= probability;
        }

        public bool getRandomBool()
        {
            return getRandomBool(0.5f);
        }
    }
}