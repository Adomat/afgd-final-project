using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;

namespace ISA.CityGeneration
{
    public class BuildingMeshGenerator
    {
        private static readonly Vector2 halfVector2 = Vector2.one / 2f;


        internal static Mesh GenerateMeshForVoronoiSite(VoronoiDiagramGeneratedSite<VoronoiSiteData> site, float height, float uvScale)
        {
            Mesh mesh = new Mesh();
            mesh.Clear();
            mesh.subMeshCount = 3;

            // 2 = top center + bottom center
            // per vertex:
            // - 2 for top and bottom of one wall triangle
            // - 2 for top and bottom of neighbar wall triangle
            // - 2 for top and bottom of floor and ceiling
            Vector3[] vertices = new Vector3[2 + site.Vertices.Count * (2 + 2 + 2)];
            Vector2[] uvs = new Vector2[vertices.Length];

            // Floor centroid
            //vertices[0] = CityGenerator.ConvertVoronoiSpaceToWorldSpace(site.Centroid) + Vector3.up * yOffset;
            vertices[0] = Vector3.zero;
            // Ceiling centroid
            vertices[1] = Vector3.up * height;
            uvs[0] = Vector2.one / 2f;
            uvs[1] = uvs[0];

            int vertexIndex = 2;
            int uvIndex = 2;



            int[] wallTriangles = new int[site.Vertices.Count * 6];
            int triangleIndex = 0;

            // Generate Walls
            for (int i = 0; i < site.Vertices.Count; i++)
            {
                Vector2 vertex = site.Vertices[i] - site.Centroid;
                Vector2 nextVertex = site.Vertices[(i + 1) % site.Vertices.Count] - site.Centroid;

                Vector3 edgeStart = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(nextVertex);
                Vector3 edgeEnd = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(vertex);

                // Vertices
                vertices[vertexIndex++] = edgeStart;
                vertices[vertexIndex++] = edgeStart + Vector3.up * height;
                vertices[vertexIndex++] = edgeEnd;
                vertices[vertexIndex++] = edgeEnd + Vector3.up * height;

                // UVs
                uvs[uvIndex++] = new Vector2(1, 0);
                uvs[uvIndex++] = new Vector2(1, 1);
                uvs[uvIndex++] = new Vector2(0, 0);
                uvs[uvIndex++] = new Vector2(0, 1);

                float width = Vector3.Distance(edgeStart, edgeEnd);
                float ratioWidthByHeight = width / height;
                if (ratioWidthByHeight < 1)
                {
                    // higher
                    uvs[uvIndex - 4].x = 0.5f + ratioWidthByHeight / 2f;
                    uvs[uvIndex - 3].x = 0.5f + ratioWidthByHeight / 2f;
                    uvs[uvIndex - 2].x = 0.5f - ratioWidthByHeight / 2f;
                    uvs[uvIndex - 1].x = 0.5f - ratioWidthByHeight / 2f;

                    // Scale UVs according to segment height so that all textures are equally sized :)
                    uvs[uvIndex - 4] = scaleUV(uvs[uvIndex - 4], height * uvScale);
                    uvs[uvIndex - 3] = scaleUV(uvs[uvIndex - 3], height * uvScale);
                    uvs[uvIndex - 2] = scaleUV(uvs[uvIndex - 2], height * uvScale);
                    uvs[uvIndex - 1] = scaleUV(uvs[uvIndex - 1], height * uvScale);
                }
                else
                {
                    // wider
                    uvs[uvIndex - 4].y = 0.5f - 1 / ratioWidthByHeight / 2f;
                    uvs[uvIndex - 3].y = 0.5f + 1 / ratioWidthByHeight / 2f;
                    uvs[uvIndex - 2].y = 0.5f - 1 / ratioWidthByHeight / 2f;
                    uvs[uvIndex - 1].y = 0.5f + 1 / ratioWidthByHeight / 2f;

                    // Scale UVs according to segment width so that all textures are equally sized :)
                    uvs[uvIndex - 4] = scaleUV(uvs[uvIndex - 4], width * uvScale);
                    uvs[uvIndex - 3] = scaleUV(uvs[uvIndex - 3], width * uvScale);
                    uvs[uvIndex - 2] = scaleUV(uvs[uvIndex - 2], width * uvScale);
                    uvs[uvIndex - 1] = scaleUV(uvs[uvIndex - 1], width * uvScale);
                }

                // Triangle #1
                wallTriangles[triangleIndex++] = vertexIndex - 4;
                wallTriangles[triangleIndex++] = vertexIndex - 2;
                wallTriangles[triangleIndex++] = vertexIndex - 1;

                // Triangle #2
                wallTriangles[triangleIndex++] = vertexIndex - 1;
                wallTriangles[triangleIndex++] = vertexIndex - 3;
                wallTriangles[triangleIndex++] = vertexIndex - 4;
            }



            int[] ceilingTriangles = new int[site.Vertices.Count * 3];
            triangleIndex = 0;
            float largestDimension = 0;

            // Generate Ceiling
            {
                //Debug.Log("Generating Ceiling!");
                // Vertices
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    Vector2 zeroCenteredVoronoiVertex = site.Vertices[i] - site.Centroid;
                    Vector3 zeroCenteredvertex = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(zeroCenteredVoronoiVertex) + Vector3.up * height;
                    vertices[vertexIndex++] = zeroCenteredvertex;
                    uvs[uvIndex++] = new Vector2(zeroCenteredvertex.x, zeroCenteredvertex.z);
                }

                // Adjust UV layout
                // Find largest dimension
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    if (largestDimension < Mathf.Abs(uvs[uvIndex - (i + 1)].x))
                        largestDimension = Mathf.Abs(uvs[uvIndex - (i + 1)].x);
                    if (largestDimension < Mathf.Abs(uvs[uvIndex - (i + 1)].y))
                        largestDimension = Mathf.Abs(uvs[uvIndex - (i + 1)].y);
                }
                // Scale UVs
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    // Scale it to lay perfectly in UV bounds
                    uvs[uvIndex - (i + 1)] = (uvs[uvIndex - (i + 1)] / largestDimension / 2f) + 0.5f * Vector2.one;
                    // Adjust scale sothat it matches UV scale of rest of building
                    uvs[uvIndex - (i + 1)] = scaleUV(uvs[uvIndex - (i + 1)], largestDimension * 2 * uvScale);
                }

                // Generate Ceiling Triangles
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    ceilingTriangles[triangleIndex++] = 1;
                    ceilingTriangles[triangleIndex++] = vertexIndex - (i + 1);
                    ceilingTriangles[triangleIndex++] = vertexIndex - ((i + 1) % site.Vertices.Count + 1);
                }
            }



            int[] floorTriangles = new int[site.Vertices.Count * 3];
            triangleIndex = 0;

            // Generate Floor
            {
                // Vertices
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    Vector2 zeroCenteredVoronoiVertex = site.Vertices[i] - site.Centroid;
                    Vector3 zeroCenteredvertex = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(zeroCenteredVoronoiVertex);
                    vertices[vertexIndex++] = zeroCenteredvertex;
                    // UV Layout will already be scaled and adjusted
                    uvs[uvIndex++] = uvs[uvIndex - site.Vertices.Count - 1];
                    // Just flip it around sothat the texture won't be mirrored :)
                    uvs[uvIndex - 1].x = -1f * uvs[uvIndex - 1].x + 1f;
                }

                // Generate Ceiling Triangles
                for (int i = 0; i < site.Vertices.Count; i++)
                {
                    floorTriangles[triangleIndex++] = 0;
                    floorTriangles[triangleIndex++] = vertexIndex - ((i + 1) % site.Vertices.Count + 1);
                    floorTriangles[triangleIndex++] = vertexIndex - (i + 1);
                }
            }



            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.SetTriangles(wallTriangles, 0);
            mesh.SetTriangles(ceilingTriangles, 1);
            mesh.SetTriangles(floorTriangles, 2);

            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
            return mesh;
        }

        private static Vector2 scaleUV(Vector2 uv, float scale)
        {
            return halfVector2 + (uv - halfVector2) * scale;
        }



        public static void ResizeUVs(Mesh mesh, float scale)
        {
            Vector2[] newUVs = mesh.uv;
            for (int i = 0; i < newUVs.Length; i++)
            {
                newUVs[i].x = 0.5f + (mesh.uv[i].x - 0.5f) * scale;
            }
            mesh.uv = newUVs;
            mesh.RecalculateUVDistributionMetrics();
        }

    }
}