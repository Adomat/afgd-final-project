using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using System;

namespace ISA.CityGeneration
{
    public class BuildingFloor : AbstractBuildingComposite
    {
        public enum FloorType
        {
            GroundFloor,
            LargeWindows, // Default!
            SmallWindows,
            NoWindows
        }



        public Building ParentBuilding { get; private set; }
        public float Index { get; internal set; }



        public float Diameter { get; internal set; }
        public float Height { get; internal set; }
        public float yOffset { get; internal set; }


        public FloorType type { get; internal set; }



        public BuildingFloor(Building Parent)
        {
            ParentBuilding = Parent;
            Index = 0;
            Diameter = 1f;
            yOffset = 0f;
            type = FloorType.LargeWindows;
        }



        public void GenerateMesh(GameObject parentObject)
        {
            GameObject floorObject = new GameObject("Floor #" + Index + " (" + type + ")");
            floorObject.transform.parent = parentObject.transform;
            floorObject.transform.position = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(ParentBuilding.VoronoiSite.Centroid) + Vector3.up * (yOffset);

            Mesh mesh = BuildingMeshGenerator.GenerateMeshForVoronoiSite(ParentBuilding.VoronoiSite, Height, 10f);
            floorObject.AddComponent<MeshFilter>().mesh = mesh;

            //Material mat2 = Resources.Load("City/DefaultBuildingWall", typeof(Material)) as Material;
            Material[] materials = new Material[3];
            materials[0] = CityGenerator.InstantiateMaterial("City/BuildingFacade/BuildingFacadeMaterial");
            materials[0].SetColor("StripeColor", ParentBuilding.Color);
            materials[0].SetInt("ShowStripes", 0);
            materials[1] = CityGenerator.InstantiateMaterial("City/BuildingFacade/BuildingRoofMaterial");
            materials[2] = CityGenerator.InstantiateMaterial("City/BuildingFacade/BuildingRoofMaterial");
            floorObject.AddComponent<MeshRenderer>().materials = materials;

            float scale = Diameter * CityGenerator.Convert01LengthToWorldSpace(CityGenerator.VORONOI_DISTANCE_01) * (2f / (mesh.bounds.size.x + mesh.bounds.size.z));
            BuildingMeshGenerator.ResizeUVs(mesh, scale);
            floorObject.transform.localScale = scale * new Vector3(1, 0, 1) + Vector3.up;
            if (type != FloorType.GroundFloor)
                floorObject.transform.rotation = Quaternion.Euler(0, RandomFactory.INSTANCE.getRandomFloat(360f), 0);

            if(type == FloorType.GroundFloor)
            {
                generateGroundFloor(floorObject);
            }
            if (type == FloorType.SmallWindows)
            {
                generateSmallWindows(floorObject);
            }
            else if (type == FloorType.LargeWindows)
            {
                generateLargeWindows(floorObject);
            }
            else if (type == FloorType.NoWindows)
            {
                materials[0].SetInt("ShowStripes", 1);
            }
        }

        private void generateGroundFloor(GameObject floorObject)
        {
            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = floorObject;
            settings.site = ParentBuilding.VoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/Windows/Door";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.05f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.absolutePosition = new Vector2(0f, 0.015f);
            /*settings.spaceBetweenPrefabsInArray = 2*.02f;
            settings.randomPlacementQuotaInArray = 1f;*/
            ObjectPlacementHelper.PlaceSingleObjectsOnBuildingFacade(settings);
        }

        private void generateLargeWindows(GameObject floorObject)
        {
            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = floorObject;
            settings.site = ParentBuilding.VoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/SatelliteDish/SatelliteDish";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.032f;
            settings.relativePosition = new Vector2(1f, 1f);
            //settings.absolutePosition = new Vector2(-.015f, -.015f);
            settings.absolutePosition = new Vector2(0f, -.015f);
            ObjectPlacementHelper.PlaceSingleObjectsOnBuildingFacade(settings);

            settings.pathToPrefab = "City/Windows/LargePanoramaWindow";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.01f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.spaceBetweenPrefabsInArray = .02f;
            settings.randomPlacementQuotaInArray = 1f;

            float spaceBetweenRows = 0.03f;
            int numberOfFittingRows = (int)Mathf.Floor((Height - 0.02f - spaceBetweenRows / 2f) / spaceBetweenRows);
            for (int i = 0; i <= numberOfFittingRows; i++)
            {
                settings.absolutePosition = new Vector2(0.5f, .02f + spaceBetweenRows * i);
                ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);
            }
        }

        private void generateSmallWindows(GameObject floorObject)
        {
            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = floorObject;
            settings.site = ParentBuilding.VoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/Windows/Balcony";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.032f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.absolutePosition = new Vector2(0.5f, .02f);
            settings.spaceBetweenPrefabsInArray = .04f;
            settings.randomPlacementQuotaInArray = 0.9f;
            ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);

            settings.pathToPrefab = "City/Windows/SmallWindow";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.01f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.spaceBetweenPrefabsInArray = .015f;
            settings.randomPlacementQuotaInArray = 0.7f;

            float spaceBetweenRows = 0.03f;
            int numberOfFittingRows = (int)Mathf.Floor((Height - 0.02f - spaceBetweenRows / 2f) / spaceBetweenRows);
            for (int i = 1; i <= numberOfFittingRows; i++)
            {
                settings.absolutePosition = new Vector2(0.5f, .02f + spaceBetweenRows * i);
                ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);
            }
        }
    }
}
