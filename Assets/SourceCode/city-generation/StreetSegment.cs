
using AfGD;
using PixelsForGlory.VoronoiDiagram;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.CityGeneration
{
    public class StreetSegment
    {
        public VoronoiDiagramGeneratedSite<VoronoiSiteData> site1 { get; private set; }
        public VoronoiDiagramGeneratedSite<VoronoiSiteData> site2 { get; private set; }

        public StreetSegment predecessorSegment;
        public StreetSegment successorSegment;

        public float width { get; private set; }

        private CurveSegment curve;
        private VoronoiDiagramGeneratedSite<VoronoiSiteData> currentVoronoiSite;
        private VoronoiDiagramGeneratedSite<VoronoiSiteData> nextVoronoiSite;

        private bool wasGenerated;
        private bool wasPostProcessed;

        public Mesh mesh { get; private set; }
        public int resolution { get; private set; }



        public StreetSegment(CurveSegment curve, VoronoiDiagramGeneratedSite<VoronoiSiteData> site1, VoronoiDiagramGeneratedSite<VoronoiSiteData> site2, int resolution = 50)
        {
            this.curve = curve;
            this.site1 = site1;
            this.site2 = site2;

            this.resolution = resolution;
            wasGenerated = false;
            wasPostProcessed = false;
        }

        internal void IncreaseWidth(float amount)
        {
            width += amount;
        }


        internal bool GenerateMesh(GameObject parentObject, float streetRelevance01)
        {
            if (wasGenerated)
                return false;
            wasGenerated = true;

            GameObject gameObject = new GameObject("Street Segment (width � [0,1]: " + streetRelevance01 + ")");
            gameObject.transform.parent = parentObject.transform;

            float streetWidth = 0.01f + streetRelevance01 * 0.09f;

            mesh = new Mesh();
            mesh.Clear();

            Vector3[] vertices = new Vector3[4 * resolution];
            int[] triangles = new int[3 * 2 * resolution];

            float interval = 1.0f / resolution;
            Vector3 lastStreetCenter = curve.Evaluate(0);
            for (int i = 0; i < resolution; i++)
            {
                float u = interval * (float)(i+1);
                Vector3 currentStreetCenter = curve.Evaluate(u);

                Vector3[] subSegmentBorderPositions = getStreetBorderPositions(lastStreetCenter, currentStreetCenter, streetWidth);
                /*
                Debug.DrawLine(subSegmentBorderPositions[0], subSegmentBorderPositions[0] + Vector3.up * 0.05f, Color.HSVToRGB((1f - streetRelevance01) * 0.7f, 1f, 1f), float.MaxValue);
                Debug.DrawLine(subSegmentBorderPositions[1], subSegmentBorderPositions[1] + Vector3.up * 0.05f, Color.HSVToRGB((1f - streetRelevance01) * 0.7f, 1f, 1f), float.MaxValue);
                Debug.DrawLine(subSegmentBorderPositions[2], subSegmentBorderPositions[2] + Vector3.up * 0.05f, Color.HSVToRGB((1f - streetRelevance01) * 0.7f, 1f, 1f), float.MaxValue);
                Debug.DrawLine(subSegmentBorderPositions[3], subSegmentBorderPositions[3] + Vector3.up * 0.05f, Color.HSVToRGB((1f - streetRelevance01) * 0.7f, 1f, 1f), float.MaxValue);
                */
                vertices[i * 4 + 0] = subSegmentBorderPositions[0];
                vertices[i * 4 + 1] = subSegmentBorderPositions[1];
                vertices[i * 4 + 2] = subSegmentBorderPositions[2];
                vertices[i * 4 + 3] = subSegmentBorderPositions[3];

                // Triangle #1
                triangles[i * 3 * 2 + 0] = i * 4 + 0;
                triangles[i * 3 * 2 + 1] = i * 4 + 1;
                triangles[i * 3 * 2 + 2] = i * 4 + 2;

                // Triangle #2
                triangles[i * 3 * 2 + 3] = i * 4 + 1;
                triangles[i * 3 * 2 + 4] = i * 4 + 3;
                triangles[i * 3 * 2 + 5] = i * 4 + 2;

                //Debug.DrawLine(lastStreetCenter, currentStreetCenter, Color.HSVToRGB((1f - streetRelevance01) * 0.7f, 1f, 1f), float.MaxValue);
                lastStreetCenter = currentStreetCenter;
            }


            mesh.vertices = vertices;
            mesh.triangles = triangles;

            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();

            gameObject.AddComponent<MeshFilter>().mesh = mesh;

            Material mat = CityGenerator.InstantiateMaterial("City/Streets/StreetMaterial");
            gameObject.AddComponent<MeshRenderer>().material = mat;
            
            return true;
        }

        private Vector3[] getStreetBorderPositions(Vector3 lastStreetCenter, Vector3 currentStreetCenter, float streetWidth)
        {
            Vector3 direction = currentStreetCenter - lastStreetCenter;
            Vector3 normal = Vector3.Cross(Vector3.up, direction.normalized).normalized;

            Vector3[] result = new Vector3[4];

            result[0] = lastStreetCenter + normal * streetWidth;
            result[1] = lastStreetCenter - normal * streetWidth;

            result[2] = currentStreetCenter + normal * streetWidth;
            result[3] = currentStreetCenter - normal * streetWidth;

            return result;
        }



        internal void PostProcessMesh()
        {
            if (wasPostProcessed)
                return;
            wasPostProcessed = true;

            Vector3[] updatedVertices = mesh.vertices;

            // Make smooth transitions between segments
            {
                if (predecessorSegment != null)
                {
                    updatedVertices[0] = predecessorSegment.mesh.vertices[predecessorSegment.resolution*4 - 2];
                    updatedVertices[1] = predecessorSegment.mesh.vertices[predecessorSegment.resolution*4 - 1];
                }
                if (successorSegment != null)
                {
                    updatedVertices[resolution * 4 - 2] = Vector3.Lerp(mesh.vertices[resolution * 4 - 2], successorSegment.mesh.vertices[0], 0.5f);
                    updatedVertices[resolution * 4 - 1] = Vector3.Lerp(mesh.vertices[resolution * 4 - 1], successorSegment.mesh.vertices[1], 0.5f);
                }

                updatedVertices[2] = Vector3.Lerp(mesh.vertices[2], mesh.vertices[4], 0.5f);
                updatedVertices[3] = Vector3.Lerp(mesh.vertices[3], mesh.vertices[5], 0.5f);

                for (int i = 1; i < resolution-1; i++)
                {
                    updatedVertices[i * 4 + 0] = Vector3.Lerp(mesh.vertices[i * 4 + 0], mesh.vertices[(i - 1) * 4 + 2], 0.5f);
                    updatedVertices[i * 4 + 1] = Vector3.Lerp(mesh.vertices[i * 4 + 1], mesh.vertices[(i - 1) * 4 + 3], 0.5f);
                    updatedVertices[i * 4 + 2] = Vector3.Lerp(mesh.vertices[i * 4 + 2], mesh.vertices[(i + 1) * 4 + 0], 0.5f);
                    updatedVertices[i * 4 + 3] = Vector3.Lerp(mesh.vertices[i * 4 + 3], mesh.vertices[(i + 1) * 4 + 1], 0.5f);
                }

                updatedVertices[(resolution - 1) * 4 + 0] = Vector3.Lerp(mesh.vertices[(resolution - 1) * 4 + 0], mesh.vertices[(resolution - 2) * 4 + 2], 0.5f);
                updatedVertices[(resolution - 1) * 4 + 1] = Vector3.Lerp(mesh.vertices[(resolution - 1) * 4 + 1], mesh.vertices[(resolution - 2) * 4 + 3], 0.5f);
            }

            mesh.vertices = updatedVertices;
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
        }
        internal void AdjustUpwardsOffset(float offset)
        {
            Vector3[] updatedVertices = mesh.vertices;

            // Lower side streets a tiny bit
            // Use plain stupid method where the street index determines how low the street is going to be...
            for (int i = 0; i < updatedVertices.Length; i++)
                if(updatedVertices[i].y == 0)
                    updatedVertices[i].y += offset;

            // This more sophisticated version works only for T intersections
            /*{
                if (predecessorSegment == null)
                {
                    int smoothingRadius = resolution / 2;
                    for (int i=0; i<=smoothingRadius; i++)
                    {
                        updatedVertices[i * 4 + 0].y = 0.01f * getCubicSmoothed01((float)i / smoothingRadius);
                        updatedVertices[i * 4 + 1].y = updatedVertices[i * 4].y;

                        updatedVertices[Math.Max(0, i * 4 - 1)].y = updatedVertices[i * 4].y;
                        updatedVertices[Math.Max(0, i * 4 - 2)].y = updatedVertices[i * 4].y;
                    }
                }
                if (successorSegment == null)
                {
                    int smoothingRadius = resolution / 2;
                    for (int i = 0; i <= smoothingRadius; i++)
                    {
                        updatedVertices[resolution * 4 - 1 - (i * 4 + 0)].y = 0.01f * getCubicSmoothed01((float)i / smoothingRadius);
                        updatedVertices[resolution * 4 - 1 - (i * 4 + 1)].y = updatedVertices[resolution * 4 - 1 - (i * 4)].y;

                        updatedVertices[Math.Min(resolution * 4 - 1, resolution * 4 - 1 - (i * 4 - 1))].y = updatedVertices[resolution * 4 - 1 - (i * 4)].y;
                        updatedVertices[Math.Min(resolution * 4 - 1, resolution * 4 - 1 - (i * 4 - 2))].y = updatedVertices[resolution * 4 - 1 - (i * 4)].y;

                        Debug.Log("Updated vertices: " + (resolution * 4 - 1 - (i * 4 + 0)) + ", " +
                            (resolution * 4 - 1 - (i * 4 + 1)) + ", " + 
                            (Math.Min(resolution * 4 - 1, resolution * 4 - 1 - (i * 4 - 1))) + ", and " + 
                            (Math.Min(resolution * 4 - 1, resolution * 4 - 1 - (i * 4 - 2))));
                    }
                }
            }*/

            mesh.vertices = updatedVertices;
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
        }

        /// <summary>
        /// 
        // f(x) = -x^2 + 2x - 1
        // https://www.wolframalpha.com/input/?i=y+%3D+-x%5E2+%2B+2x+-+1
        // f(0) = -1
        // f(1) = 0
        /// </summary>
        /// <param name="input01">Input in range [0, 1]</param>
        /// <returns></returns>
        private float getCubicSmoothed01(float input01)
        {
            return (-Mathf.Pow(input01, 2) + 2 * input01 - 1);
        }



        internal void SetupUVs(float uvScale)
        {
            Vector2[] uvs = new Vector2[mesh.vertices.Length];

            float length = 0;
            for (int i = 0; i < resolution-1; i++)
            {
                length += Vector3.Distance(curve.Evaluate((float)(i) / resolution), curve.Evaluate((float)(i + 1) / resolution));
            }
            
            for (int i = 0; i < resolution; i++)
            {
                Vector3 startLeft = mesh.vertices[i * 4 + 0];
                Vector3 startRight = mesh.vertices[i * 4 + 1];
                float startDistance = Vector3.Distance(startLeft, startRight);

                Vector3 endLeft = mesh.vertices[i * 4 + 2];
                Vector3 endRight = mesh.vertices[i * 4 + 3];
                float endDistance = Vector3.Distance(endLeft, endRight);

                float yStart = 0.5f - uvScale * length / (float) resolution / 2f;
                float yEnd = 0.5f + uvScale * length / (float)resolution / 2f;

                uvs[i * 4 + 0] = new Vector2(0.5f + uvScale * startDistance / 2f, yStart);
                uvs[i * 4 + 1] = new Vector2(0.5f - uvScale * startDistance / 2f, yStart);
                uvs[i * 4 + 2] = new Vector2(0.5f + uvScale * endDistance / 2f, yEnd);
                uvs[i * 4 + 3] = new Vector2(0.5f - uvScale * endDistance / 2f, yEnd);
            }

            mesh.uv = uvs;
        }
    }
}
