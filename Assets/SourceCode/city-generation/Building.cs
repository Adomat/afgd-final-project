﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;

namespace ISA.CityGeneration
{
    public class Building : AbstractBuildingComposite
    {
        private City city;


        public List<BuildingFloor> Floors { get; private set; }
        public BuildingGarden Garden { get; private set; }
        public float Height { get; private set; }
        public Color Color { get; private set; }


        // IMPORTANT!! Don't use this points after the Voronoi Floyd relaxation was run!
        public Vector2 InitialVoronoiCenterPoint;
        //public List<Vector2> SubComplexPoints { get; private set; }
        public List<Vector2> GardenPoints { get; private set; }


        internal VoronoiDiagramGeneratedSite<VoronoiSiteData> VoronoiSite;
        //internal List<VoronoiDiagramGeneratedSite<VoronoiSiteData>> ComplexVoronoiSites = new List<VoronoiDiagramGeneratedSite<VoronoiSiteData>>();
        internal List<VoronoiDiagramGeneratedSite<VoronoiSiteData>> GardenVoronoiSites = new List<VoronoiDiagramGeneratedSite<VoronoiSiteData>>();






        public Building(City city)
        {
            this.city = city;

            Height = 0f;
            Color = Color.HSVToRGB(RandomFactory.INSTANCE.getRandomFloat(), RandomFactory.INSTANCE.getRandomFloat(0.5f, 0.9f), RandomFactory.INSTANCE.getRandomFloat(0.25f, 0.9f));

            // add info for building floors!
            Floors = new List<BuildingFloor>();
            BuildingFloor groundFloor = new BuildingFloor(this);
            groundFloor.type = BuildingFloor.FloorType.GroundFloor;
            groundFloor.Height = RandomFactory.INSTANCE.getRandomFloat(0.05f, 0.2f);
            Height += groundFloor.Height;
            Floors.Add(groundFloor);

            for (int i=1; i< RandomFactory.INSTANCE.getRandomFloat(1, 20); i++)
            {
                BuildingFloor newFloor = new BuildingFloor(this);
                newFloor.Index = i;
                newFloor.Diameter = Mathf.Pow(RandomFactory.INSTANCE.getRandomFloat(.5f, 1f), 2);

                newFloor.Height = RandomFactory.INSTANCE.getRandomFloat(0.05f, 0.2f);
                newFloor.yOffset = Height;
                Height += newFloor.Height;

                //newFloor.NumberOfWindows = (int) Mathf.Ceil(Mathf.Pow(Random.Range(0f, 20f), 2) / 80f);
                if(RandomFactory.INSTANCE.getRandomFloat() > 0.4f)
                {
                    if (RandomFactory.INSTANCE.getRandomFloat() > 0.4f)
                        newFloor.type = BuildingFloor.FloorType.LargeWindows;
                    else
                        newFloor.type = BuildingFloor.FloorType.SmallWindows;
                }
                else
                {
                    newFloor.type = BuildingFloor.FloorType.NoWindows;
                }

                Floors.Add(newFloor);
            }
        }

        public void GenerateGarden()
        {
            int numberOfGardenSites = RandomFactory.INSTANCE.getRandomInt(5, 8);
            GardenPoints = new List<Vector2>();
            for (int i = 0; i < numberOfGardenSites; i++)
            {
                Vector2 relativePoint = CityGenerator.CalculatePositionInCircle01(i, numberOfGardenSites);
                Vector2 absolutePoint = InitialVoronoiCenterPoint + relativePoint * CityGenerator.VORONOI_DISTANCE_01;
                GardenPoints.Add(absolutePoint);
            }
        }

        /*public static float CalculateInnerCircleRemapping(int size)
        {
            float relativeSize = (float)(size - MIN_SUB_COMPLEXES) / (MAX_SUB_COMPLEXES - MIN_SUB_COMPLEXES);
            return MAX_SIZE_ON_VORONOI * (0.2f + relativeSize * 0.8f);
        }

        public static float CalculateOuterCircleRemapping(int size)
        {
            return 1.5f * CalculateInnerCircleRemapping(size);
        }*/






        public void GenerateMesh(GameObject parentObject)
        {
            GameObject buildingObject = new GameObject("Building");
            buildingObject.transform.parent = parentObject.transform;

            foreach(BuildingFloor floor in Floors)
            {
                floor.GenerateMesh(buildingObject);
            }
        }
    }
}